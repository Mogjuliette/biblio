<?php

namespace App\Entities;

class Abonne {

    private ?int $id;
    private string $prenom;
    private string $nom;

    /**
     * @param string $prenom
     * @param string $nom
     *  @param int|null $id
     */
    public function __construct(string $prenom, string $nom, ?int $id = null) {
    	$this->prenom = $prenom;
    	$this->nom = $nom;
        $this->id = $id;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPrenom(): string {
		return $this->prenom;
	}
	
	/**
	 * @param string $prenom 
	 * @return self
	 */
	public function setPrenom(string $prenom): self {
		$this->prenom = $prenom;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNom(): string {
		return $this->nom;
	}
	
	/**
	 * @param string $nom 
	 * @return self
	 */
	public function setNom(string $nom): self {
		$this->nom = $nom;
		return $this;
	}
}