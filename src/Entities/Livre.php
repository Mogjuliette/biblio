<?php

namespace App\Entities;

use DateTime;

class Livre {

    private ?int $id;

    private string $titre;

    private string $auteur;

    private DateTime $date;

    private bool $disponibilite;

    
    private ?int $id_abonne;


    /**
     * @param string $titre
     * @param string $auteur
     * @param DateTime $date
     * @param bool $disponibilite
     * @param int|null $id_abonne
     * @param int|null $id
     */
    public function __construct(string $titre, string $auteur, DateTime $date, bool $disponibilite, ?int $id_abonne = null, ?int $id = null) {
    	$this->titre = $titre;
    	$this->auteur = $auteur;
    	$this->date = $date;
    	$this->disponibilite = $disponibilite;
        $this->id_abonne = $id_abonne;
        $this->id = $id;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTitre(): string {
		return $this->titre;
	}
	
	/**
	 * @param string $titre 
	 * @return self
	 */
	public function setTitre(string $titre): self {
		$this->titre = $titre;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAuteur(): string {
		return $this->auteur;
	}
	
	/**
	 * @param string $auteur 
	 * @return self
	 */
	public function setAuteur(string $auteur): self {
		$this->auteur = $auteur;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDate(): DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime $date 
	 * @return self
	 */
	public function setDate(DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getId_abonne(): int {
		return $this->id_abonne;
	}
	
	/**
	 * @param int $id_abonne 
	 * @return self
	 */
	public function setId_abonne(int $id_abonne): self {
		$this->id_abonne = $id_abonne;
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function getDisponibilite(): bool {
		return $this->disponibilite;
	}
	
	/**
	 * @param bool $disponibilite 
	 * @return self
	 */
	public function setDisponibilite(bool $disponibilite): self {
		$this->disponibilite = $disponibilite;
		return $this;
	}
}