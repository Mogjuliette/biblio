<?php

namespace App\Repository;

use PDO;
use App\Entities\Abonne;

class AbonneRepository {

    private PDO $connection;

    public function __construct() {
    	$this->connection = new PDO('mysql:host=localhost;dbname=p23_exo_Rama', 'simplon', '1234');
    }

    public function persist(Abonne $abonne){
        $statement = $this->connection->prepare('INSERT INTO abonnes (prenom, nom) VALUES (:prenom, :nom)');
        $statement->bindValue('prenom', $abonne->getPrenom(), PDO::PARAM_STR);
        $statement->bindValue('nom', $abonne->getNom(), PDO::PARAM_STR);
        

        $statement->execute();

        $abonne->setId($this->connection->lastInsertId());  
    }

    public function findAll(): array{
    
        $abonnes = [];

        $statement = $this->connection->prepare('SELECT * FROM abonnes');
        
        $statement->execute();
        
        $results = $statement->fetchAll();
        
        foreach($results as $abonne){
            $abonnes[] = new Abonne($abonne['prenom'], $abonne['nom'], $abonne['id']);
        }
        return $abonnes;
    } 

    public function findById(int $id){
        $statement = $this->connection->prepare('SELECT * FROM abonnes WHERE id = :id');
        
        $statement->bindValue('id', $id, PDO::PARAM_INT);

        $statement->execute();
        
        $result = $statement->fetch();
        
        return  new Abonne($result['prenom'], $result['nom'], $result['id']);
    }
}