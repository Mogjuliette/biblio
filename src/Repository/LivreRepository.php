<?php

namespace App\Repository;

use App\Entities\Abonne;
use PDO;
use App\Entities\Livre;
use DateTime;

class LivreRepository {

    private PDO $connection;

    public function __construct() {
    	$this->connection = new PDO('mysql:host=localhost;dbname=p23_exo_Rama', 'simplon', '1234');
    }

    public function persist(Livre $livre){
        $statement = $this->connection->prepare('INSERT INTO livres (titre, auteur, date, disponibilite) VALUES (:titre, :auteur, :date, :disponibilite)');
        $statement->bindValue('titre', $livre->getTitre());
        $statement->bindValue('auteur', $livre->getAuteur());
        $statement->bindValue('date', $livre->getDate()->format('Y-m-d'));
        $statement->bindValue('disponibilite', $livre->getDisponibilite());

        $statement->execute();

        $livre->setId($this->connection->lastInsertId());
    }

    public function findAll(): array{
    
        $livres = [];

        $statement = $this->connection->prepare('SELECT * FROM livres');
        
        $statement->execute();
        
        $results = $statement->fetchAll();
        
        foreach($results as $livre){
            $livres[] = new Livre($livre['titre'], $livre['auteur'], new DateTime($livre['date']), $livre['disponibilite'], $livre['id_abonne'], $livre['id']);
        }
        return $livres;
    } 

    public function deleteById(int $id){
        $statement = $this->connection->prepare('DELETE FROM livres WHERE id = :id');

        $statement->bindValue('id', $id);

        $statement->execute();
    }

    public function update(Livre $livre){
        $statement = $this->connection->prepare('UPDATE livres SET titre = :titre, auteur = :auteur, date = :date, disponibilite = :disponibilite, id_abonne = :id_abonne WHERE id = :id');

        $statement->bindValue('id', $livre->getId(), PDO::PARAM_INT);
        $statement->bindValue('titre', $livre->getTitre(), PDO::PARAM_STR);
        $statement->bindValue('auteur', $livre->getAuteur(), PDO::PARAM_STR);
        $statement->bindValue('date', $livre->getDate()->format("Y-m-d"), PDO::PARAM_STR);
        $statement->bindValue('disponibilite', $livre->getDisponibilite(), PDO::PARAM_BOOL);
        $statement->bindValue('id_abonne', $livre->getId_abonne(), PDO::PARAM_INT);

        $statement->execute();
    }

    public function updateDisponiliteById(int $id){
        $book = $this->findById($id);
        $book->getDisponibilite() == true ? $newdisponibilite = false : $newdisponibilite = true;
        $statement = $this->connection->prepare('UPDATE livres SET disponibilite = :valeur_colonne WHERE id = :id');

        $statement->bindValue('id', $id);
        $statement->bindValue('valeur_colonne', $newdisponibilite, PDO::PARAM_BOOL);

        $statement->execute();
    }

    public function findById(int $id){
        $statement = $this->connection->prepare('SELECT * FROM livres WHERE id = :id');
        
        $statement->bindValue('id', $id, PDO::PARAM_INT);

        $statement->execute();
        
        $result = $statement->fetch();
        
        
        return  new Livre($result['titre'], $result['auteur'], new DateTime($result['date']), $result['disponibilite'], $result['id_abonne'], $result['id']);
        
    }

    public function reservation(Abonne $abonne, Livre $livre){
        $statement = $this->connection->prepare('UPDATE livres SET disponibilite = 0, id_abonne = :id_abonne WHERE id = :id');

        $statement->bindValue('id', $livre->getId(), PDO::PARAM_INT);
        $statement->bindValue('id_abonne', $abonne->getId(), PDO::PARAM_INT);

        $statement->execute();
        
    }

    public function showReservation(Livre $livre) {
        $statement = $this->connection->prepare('SELECT abonnes.prenom, abonnes.nom from abonnes JOIN livres ON abonnes.id = livres.id_abonne WHERE livres.id = :id');
        $statement->bindValue('id', $livre->getId(), PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();
        return $result;

    }

    public function finReservation(Livre $livre){
        $statement = $this->connection->prepare('UPDATE livres SET disponibilite = 1, id_abonne = null WHERE id = :id');

        $statement->bindValue('id', $livre->getId(), PDO::PARAM_INT);

        $statement->execute();
    }
}