-- Active: 1674203942125@@127.0.0.1@3306@p23_exo_Rama
DROP TABLE IF EXISTS abonnes;

CREATE TABLE abonnes(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    prenom VARCHAR(255),
    nom VARCHAR(255)
);

INSERT INTO abonnes (prenom, nom) VALUES ('Juliette','Suc'),('Fae','Mayot'),('Elodie','Bouhier'),('Laureline','Pinault');
DROP TABLE IF EXISTS livres;

CREATE TABLE livres(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    titre VARCHAR(255),
    auteur VARCHAR(255),
    date DATE,
    disponibilite BOOLEAN,
    id_abonne int,
    Foreign Key (id_abonne) REFERENCES abonnes (id)
);

INSERT INTO livres (titre, auteur, date, disponibilite) VALUES ('Cyrano de Bergerac', 'Rostand','1897-01-01' , 1),('Le château de Hurles','Diana Wynne Jones','1980-01-01',1),('Les Dépossédés','Usrula le Guin','1975-01-01',1),('Le régiment monstrueux','Terry Pratchett','2010-01-01',1);

SELECT * from abonnes JOIN livres ON abonnes.id = livres.id_abonne WHERE livres.id = 4;
